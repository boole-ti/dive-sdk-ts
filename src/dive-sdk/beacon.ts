import Event from "./event";
import 'whatwg-fetch';

class Beacon extends Event {

	public constructor() {
		super("beacon",{});
	}
}

export default Beacon;