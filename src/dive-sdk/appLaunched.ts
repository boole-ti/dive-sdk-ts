import Event from './event';
import Utils from './utils';
import Globals from './globals';
import 'whatwg-fetch';

var parser = require('ua-parser-js');

interface ExtraParams {
	query_string: string,
	referrer: string
}

class AppLaunched extends Event {

	public constructor() {
		const userAgent = parser(navigator.userAgent);
		
		const device = (userAgent.device.vendor || userAgent.device.model) ? userAgent.device.vendor + " " + userAgent.device.model : userAgent.cpu.architecture?  'pc' : 'unknown';
		
		let cookie;
		if (!(cookie = Utils.getCookie("COOKIEID"))) {
			Utils.setCookie("COOKIEID", (cookie = Utils.uuidv4()));
		}

		// navigator.
		super("appLaunched", {
			device: device,
			user_agent: navigator.userAgent,
			connection: "",
			id_type: Globals.id_type,
			id_value: cookie,
			lang: navigator.language,
			os_version: navigator.platform,
			is_refreshed: 0,
			extra_params: {query_string:"",referrer:""},
			c_id: null,
		});
	}

	public getDevice(): string {
		return this.getEventBody().getJson().device;
	};
	
	public getUserAgent(): string {
		return this.getEventBody().getJson().user_agent;
	};
	
	public getConnection(): string {
		return this.getEventBody().getJson().connection;
	};
	public setConnection(connection:string):AppLaunched {
		this.getEventBody().getJson().connection = connection;
		return this;
	};

	public getIdType(): string {
		return this.getEventBody().getJson().id_type;
	};
	
	public getIdValue(): string {
		return this.getEventBody().getJson().id_value;
	};
	
	public getLang(): string {
		return this.getEventBody().getJson().lang;
	};
	
	public getOsVersion(): string {
		return this.getEventBody().getJson().os_version;
	};
	
	public setIsRefreshed(is_refreshed: number): AppLaunched {
		this.getEventBody().getJson().is_refreshed = is_refreshed;
		return this;
	};
	
	public getIsRefreshed(): number {
		return this.getEventBody().getJson().is_refreshed;
	}

	private isExtraParams(object: any): object is ExtraParams {
		return 'query_string' in object || 'referrer' in object;
	}

	public setExtraParams(p1: string | ExtraParams, p2?: string): AppLaunched {
		this.getEventBody().getJson().extra_params = this.isExtraParams(p1) ? p1 : { query_string: p1, referrer: p2 };
		return this;
	};

	public setExtraParamQueryString(query_string: string): AppLaunched {
		this.getEventBody().getJson().extra_params = Object.assign(this.getEventBody().getJson().extra_params, { query_string: query_string });
		return this;
	};
	public setExtraParamReferrer(referrer: string): AppLaunched {
		this.getEventBody().getJson().extra_params = Object.assign(this.getEventBody().getJson().extra_params, { referrer: referrer });
		return this;
	};

	public getExtraParams(): ExtraParams {
		return this.getEventBody().getJson().extra_params;
	};

	public setCId(cid: string): AppLaunched {
		this.getEventBody().getJson().cid = cid;
		return this;
	};
	public getCId(): string {
		return this.getEventBody().getJson().cid;
	};
}

export default AppLaunched;