import Utils from './utils';
import Globals from './globals';
import EventBody from './eventbody';
import 'whatwg-fetch';

interface IHeader {
	name: string;
	player_level: string;
	player_id: string;
	s_sessionid: string;
	game_ver: string;
	app_token: string;
	platform: string;
	env: string;
	c_sessionid:string;
	uuid:string;
	timezone:string;
	ts:string;
	unixts:number;
	EventBody:EventBody,
	[key: string]: any;
}


class Event{
	
	protected header = {
		name: "",
		player_level: "",
		player_id: "",
		s_sessionid: "",
		game_ver: "",
		app_token: "",
		platform: Globals.platform,
		env: "dev",
		c_sessionid: Utils.getClientSessionId(),
		uuid: Utils.uuidv4(),
		timezone: new Date().toString().split("GMT")[1].split(" (")[0],
		ts:"",
		unixts: 0,
		body: {}
	}

	protected event: IHeader;
	protected eventBody: EventBody;

	public constructor(name:string,body:EventBody|{}) {
		const now = new Date();
		
		this.header.ts = new Date(+now + now.getTimezoneOffset() * 6e4).toISOString();
		this.header.unixts = Date.UTC(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate(), now.getUTCHours(), now.getUTCMinutes(), now.getUTCSeconds());
		
		this.header.name = name;

		if (body instanceof EventBody) {
			this.eventBody = body;
		} else {
			this.eventBody = new EventBody(body);
		}
	}

	public _setTs(ts:string):Event {
		this.header.ts = ts;
		return this;
	}
	public _setUnixts(unixts:number):Event {
		this.header.unixts = unixts;
		return this;
	}

	public getEventBody():EventBody {
		return this.eventBody;
	}

	public setEventBody(eventBody:EventBody|{}): Event {

		if (eventBody instanceof EventBody) {
			this.eventBody = eventBody;
		} else {
			this.eventBody = new EventBody(eventBody);
		}

		return this;
	}
	
	public toJson():string {
		this.header.body = this.eventBody.getJson();
		return JSON.stringify(this.header);
	}

	public setAppToken(app_token:string):Event {
		this.header.app_token = app_token;
		return this;
	}

	public setName(name:string): Event {
		this.header.name = name;
		return this;
	}
	public getName(): string {
		return this.header.name;
	}

	public setPlayerLevel(player_level:string):Event {
		this.header.player_level = player_level;
		return this;
	}
	public getPlayerLevel():string {
		return this.header.player_level;
	}

	public setPlayerId(player_id:string): Event {
		this.header.player_id = player_id;
		return this;
	}
	public getPlayerId(): string {
		return this.header.player_id;
	}

	public setSSessionId(s_sessionid:string): Event {
		this.header.s_sessionid = s_sessionid;
		return this;
	}
	public getSSessionId():string {
		return this.header.s_sessionid;
	}

	public setGameVer(game_ver:string): Event {
		this.header.game_ver = game_ver;
		return this;
	}
	public getGameVer():string {
		return this.header.game_ver;
	}

	public getCSessionid():string {
		return this.header.c_sessionid;
	}

	public getPlatform():string {
		return this.header.platform;
	}

	public setEnv(env:string): Event {
		this.header.env = env;
		return this;
	}
	public getEnv():string {
		return this.header.env;
	}

	public getUuid():string {
		return this.header.uuid;
	}

	public getTimezone():string {
		return this.header.timezone;
	}

	public getUnixts():number {
		return this.header.unixts;
	}

	public getTs():string {
		return this.header.ts;
	}


	public SetCustomHeaderParam(param:string, value:any): Event {
		this.header[param] = value;
		return this;
	}
}

export default Event;