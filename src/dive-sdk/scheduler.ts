import 'whatwg-fetch';

export interface SchedulerSettings {
	repeating_time: number,
	iterations: number
}

export default class Scheduler {

	private _iteration: number = 1;
	private _interval: any;

	private state: object = {};

	private settings: SchedulerSettings;

	private static DEFAULT_SETTINGS: SchedulerSettings = {
		repeating_time: 100,
		iterations: 5
	}

	handler = (_i:number,_state:{}) => {};
	onStart = () => {};
	onEnd 	= () => {};

	constructor(handler:(iterator:any,state:{})=>void, settings?:SchedulerSettings, state?:object) {
		this.settings 	= settings 	|| Scheduler.DEFAULT_SETTINGS;
		this.handler 	= handler 	|| this.handler;
		this.state 		= state 	|| this.state;
	}
	
	run():Scheduler {
		this._interval = setInterval( ()=> {
			if(!this.settings.iterations  || this._iteration <= this.settings.iterations) {
				this.onStart.call(this,this.state);
				this.handler.call(this,this._iteration++,this.state);
			} else if(this.settings.iterations) {
				this.stop();
			}
		},this.settings.repeating_time);
		
		return this;
	}

	stop() {
		this._iteration--;
		clearInterval(this._interval);
		this.onEnd.call(this,this._iteration,this.state);
	}
}