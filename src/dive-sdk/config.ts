import Globals from "./globals";
import  Logger from "./logger";
import LogLevels from "./logLevel";
import 'whatwg-fetch';

// import "isomorphic-fetch";


interface key_value {
	[key:string]:any;
}

export default class Config {
	private static instance: Config;

	private token: string;
	private data: key_value = {};

	private results = false;

	private constructor(token:string) {
		this.token = token;
	}

	public static getInstance(token?:string): Config {
	
		if(!Config.instance && token) {
			Config.instance = new Config(token);
		}

		return Config.instance;
	}

	public async init() {
		return fetch(`${Globals.config_url}${this.token}.json`,{
			mode: 'cors',
			headers: {
				'origin':window.location.origin,
			}
		})
			.then(result=>result.json())
			.then(result => {
				this.data = result[Globals.platform] as JsonConfig;
				this.results = true;	
			}).catch(e=> {
				Logger.getInstance().add(`Can not retrieve config data from "${Globals.config_url}${this.token}.json". Server return status code ${e.status}`, LogLevels.ERROR)
				this.results = false;
			});
	}

	public hasResults():boolean {
		return this.results;
	}

	public get(key:string, defaultValue?:any):any {
		return this.data[key] ? this.data[key] : (defaultValue ? defaultValue : null);
	}
}

export interface JsonConfig {
	is_sdk_enabled: number,
    is_beacon_enabled: number,
    beacon_freq_sec: number,
    tracking_url: string,
    retries_no: number,
    retries_sleep_sec: number
}