import 'whatwg-fetch';

class Globals {
	public static config_url  		= 'https://public.dive.games/sdk/';
	//public static config_url  		= 'http://localhost/dive/sdk-config/';
	public static events_path 		= '/events';
	public static id_type			= "COOKIEID";
	public static platform			= 'web';
	public static SESSION_ID_KEY	= 'suid';
	
}

export default Globals;