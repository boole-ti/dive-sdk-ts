import 'whatwg-fetch';

interface IEventBody {
	[key:string]:any
}

export default class EventBody {
	
	protected json: IEventBody;

	public constructor(EventBody:IEventBody) {
		this.json = EventBody;
	}

	public getJson() {
		return this.json;
	}

	public addParam(name:string,value:any):EventBody {
		this.json[name] = value;
		return this;
	}
}