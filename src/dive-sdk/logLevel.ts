import 'whatwg-fetch';

export interface LogLevel {
	code: number,
	name: string,
	style: string|null
}

export default class LogLevels {
	public static DISABLED: LogLevel 	= {code: 0,name: "DISABLED", style:null};
	public static ERROR: LogLevel 		= {code: 1,name: "ERROR", style:'background: #bc1601; color: #fafafa'};
	public static INFO: LogLevel 		= {code: 2,name: "INFO", style: 'background: #BFE8A5; color: #1a1a1a'};
	public static DEBUG: LogLevel 		= {code: 3,name: "DEBUG", style:null};
}