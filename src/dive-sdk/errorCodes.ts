import 'whatwg-fetch';

class ErrorCode {
	private message:string;
	private code:number;
	constructor(message:string,code:number) {
		this.message = message;
		this.code = code;
	}

	public getCode():number {
		return this.code;
	}

	public getMessage():string {
		return this.message;
	}
}

class ErrorCodes {
	public INVALID_TOKEN: ErrorCode 		= new ErrorCode("Invalid app token",1);
	public INVALID_EVENT_DATA: ErrorCode 	= new ErrorCode("Invalid event data",2);
	public INVALID_SETTINGS: ErrorCode 		= new ErrorCode("Invalid settings",3);
}

export {ErrorCode,ErrorCodes};