import Logger, {LoggerSettings} from './logger';
import LogLevels from './logLevel';
import Scheduler from './scheduler';
import Config from './config';
import SdkStarted from './sdkStarted';
import Event from './event';
import Globals from './globals';
import Utils from './utils';
import Beacon from './beacon';
import 'whatwg-fetch';


interface DiveSettings {
	logger: LoggerSettings,
	hash_key: string
}

class DiveSdk {

	private static instance: DiveSdk;

	private token: string;
	private env: string = "env";
	private game_ver: string = "";
	private player_level: string = "";
	private player_id: string = "";
	private s_sessionid: string = "";

	private settings: DiveSettings;
	private static DEFAULT_SETTINGS: DiveSettings = {
		logger: {
			level: LogLevels.DEBUG,
		},
		hash_key: ""
	}

	beaconScheduler:Scheduler = new Scheduler(()=>{});

	private constructor(token:string, settings:DiveSettings)  {
		this.token 		= token;
		this.settings 	= settings;

		Logger.getInstance(this.settings.logger);
		Config.getInstance(this.token).init().then( () =>this.init());
		
	}

	public static getInstance(token: string|null, settings: DiveSettings|null): DiveSdk {
		if(!DiveSdk.instance) {
			if(!token) {
				Logger.getInstance().add("You must to set app token", LogLevels.ERROR);
			} else {
				DiveSdk.instance = new DiveSdk(token, settings||this.DEFAULT_SETTINGS);
			}
		}

		return DiveSdk.instance;
	}

	private init() {
		if(Utils.firstSession()) {
			this.RecordEvent(new SdkStarted());
		}
		this.initBeaconScheduler();
	}

	public setEnv(env:string):DiveSdk {
		this.env = env;
		return this;
	}

	public setGameVer(game_ver:string):DiveSdk {
		this.game_ver = game_ver;
		return this;
	}
	public setPlayerId(player_id:string):DiveSdk {
		this.player_id = player_id;
		return this;
	}
	public setPlayerLevel(player_level:string):DiveSdk {
		this.player_level = player_level;
		return this;
	}
	public setServerSessionId(s_sessionid:string):DiveSdk {
		this.s_sessionid = s_sessionid;
		return this;
	}

	private initBeaconScheduler() {
		if(Config.getInstance().get("is_beacon_enabled",false)) {
			this.beaconScheduler = new Scheduler(() => this.RecordEvent(new Beacon()), 
				{iterations:0, repeating_time: Config.getInstance().get("beacon_freq_sec",5) * 1000}
			).run();
		}
	}

	private postEvent(event:Event) {
		const now = new Date();
		
		event._setTs(new Date(+now + now.getTimezoneOffset() * 6e4).toISOString());
		event._setUnixts(Date.UTC(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate(), now.getUTCHours(), now.getUTCMinutes(), now.getUTCSeconds()));
		
		

		return fetch(
			Config.getInstance().get("tracking_url") + Globals.events_path,  
			{
				method:'POST',
				credentials:'same-origin',
				body:event.toJson(),
				mode:'cors',
				headers: {
					'Authorization': `Bearer ${this.token}`, 
					'digest': Utils.digest(event, this.settings.hash_key),
					'Content-type': 'application/json',
					'origin':window.location.origin,
				}
			}
		)
		.then( ()=> {
			Logger.getInstance().add(`Event ${event.getName()} ${event.toJson()} sent`, LogLevels.INFO);
		});
	}

	public RecordEvent(param1:Event|string, param2?:{}):DiveSdk {
		if(Number.parseInt(Config.getInstance().get("is_sdk_enabled"))===1) {

			const event = param1 instanceof Event ? param1 : new Event(param1,param2 || {});
			
			try {
				if(this.token) { event.setAppToken(this.token); }
				if(this.env) { event.setEnv(this.env); }
				if(this.game_ver) { event.setGameVer(this.game_ver); }
				if(this.player_id) {event.setPlayerId(this.player_id); }
				if(this.player_level) {event.setPlayerLevel(this.player_level); }
				if(this.s_sessionid) {event.setSSessionId(this.s_sessionid); }
				
				this
					.postEvent(event)
					.catch(e=> {
						Logger.getInstance().add(`Event ${event.getName()} submission failed. Server return statusCode ${e.status}`, LogLevels.ERROR);
										
						var retries = new Scheduler(iterator => {
							Logger.getInstance().add(`Implementing event submission retries. Retrie number: ${iterator}`, LogLevels.DEBUG);
							
							this
								.postEvent(event)
								.catch(error=> {
									Logger.getInstance().add(`Event resubmission failed on retrie number: ${iterator}. Server says ${error}`, LogLevels.ERROR);
								});
						},{
							repeating_time: Config.getInstance().get("retries_sleep_sec") * 1000,
							iterations: Config.getInstance().get("retries_no")
						});
						retries.run();
						
						return this;
					});

			} catch(error) {
				Logger.getInstance().add(`Event submission failed. Error says ${error}`, LogLevels.ERROR);
			}
		}
		return this;
	}
}

export default DiveSdk;