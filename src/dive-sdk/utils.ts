import Event from './event';
import Crypt from './crypt';
import 'whatwg-fetch';

class Utils {

	private static SESSION_ID_KEY = "csid";

	public static uuidv4():string {
		return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, c => {
			var r = Math.random() * 16 | 0, v = c === 'x' ? r : (r & 0x3 | 0x8);
			return v.toString(16);
		});
	}

	public static digest(event:Event,key:string) {
		return Crypt.sha1_hmac(event.toJson(),key);
	}

	public static firstSession(): boolean {
		return (!sessionStorage.getItem(Utils.SESSION_ID_KEY));
	}

	public static getClientSessionId():string {
		if(!sessionStorage.getItem(Utils.SESSION_ID_KEY)) {
			sessionStorage.setItem(Utils.SESSION_ID_KEY,Utils.uuidv4());
		}
		return sessionStorage.getItem(Utils.SESSION_ID_KEY) as string;
	}

	public static inArray(p1:any,p2:any):boolean {
		return Array.isArray(p1) ? (p1.indexOf(p2)!==-1) : (Array.isArray(p2) ? (p2.indexOf(p1)!==-1) : false);
	}

	public static getCookie = function(name:string):any {
		var v = document.cookie.match('(^|;) ?' + name + '=([^;]*)(;|$)');
		return v ? v[2] : null;
	}

	public static setCookie = function(name:string, value:any, days?:number):void {
		let d: Date = new Date(2147483647 * 1000);
		if(days) {
			d = new Date();
			d.setTime(d.getTime() + 24*60*60*1000*days);
		}
		document.cookie = name + "=" + value + ";path=/;expires=" + d.toUTCString();
	}
}

export default Utils;