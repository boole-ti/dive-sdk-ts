import LogLevels, { LogLevel } from "./logLevel";
import 'whatwg-fetch';

export interface LoggerSettings {
	level:LogLevel;
}

export default class Logger {

	private static DEFAULT_SETTINGS: LoggerSettings = {
		level: LogLevels.DEBUG
	}
	
	private settings: LoggerSettings;

	private static instance: Logger;

	private constructor(settings: LoggerSettings) {
		this.settings = settings;
	}

	public static getInstance(settings: LoggerSettings|void): Logger {
		if(!Logger.instance) {
			Logger.instance = new Logger(settings || Logger.DEFAULT_SETTINGS);
		}

		return Logger.instance;
	}

	public add(message:string, level:LogLevel) {
		if(this.settings.level.code>=level.code) {
			console.log(`%c[${level.name}][${new Date().toISOString()}] - ${message}`, level.style);
		}
	}
}