import Event from "./event";
import 'whatwg-fetch';

class SdkStarted extends Event {

	public constructor() {
		super("sdkStarted",{});
	}
}

export default SdkStarted;